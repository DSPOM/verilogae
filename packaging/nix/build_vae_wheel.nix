{pkgs , verilogae}:

with pkgs;

{
name ? "${pname}-${version}"
, pname
, moduleName ? args.pname
, version ? lib.getVersion verilogae
, mainfile
, src
, target ? rust.toRustTargetSpec stdenv.hostPlatform
, nativeBuildInputs ? []
, ... } @ args:

let

    # Copied from nixpkgs. Required to make cross compilation work properly (no way to reuse this sadly far as I know)
    ccForBuild = "${buildPackages.stdenv.cc}/bin/${buildPackages.stdenv.cc.targetPrefix}cc";
    cxxForBuild = "${buildPackages.stdenv.cc}/bin/${buildPackages.stdenv.cc.targetPrefix}c++";
    ccForHost = "${stdenv.cc}/bin/${stdenv.cc.targetPrefix}cc";
    cxxForHost = "${stdenv.cc}/bin/${stdenv.cc.targetPrefix}c++";

    # Fetch std libary here because verilogae doesnt have the network access required
    constant_va = builtins.fetchurl {
        url = "https://www.accellera.org/images/downloads/standards/v-ams/constants_2-4.vams";
        sha256 = "3e408b8ba92e872fa134f038b2fc9fdfc0845cf0c15e6914521e18ec1ab0bfcb";
    };
    disciplines_va = builtins.fetchurl {
        url = "https://www.accellera.org/images/downloads/standards/v-ams/disciplines_2-4.vams";
        sha256 = "053a05ae1363ab8bfb2be96f7425db26a465b218c3fffabd78875be3ced4af59";
    };

    # Treat verilotgae as a two part compiler
    # First part compiles va -> build directory (behaviourl object files, Cargo toml/lock and source files)
    # Second part is a simple rust compile with special flags produced by verilogae/maturin that compiles build directory -> python wheel
    rustBuildDir = stdenvNoCC.mkDerivation {
          name = "${name}-raw-artifacts";
          inherit src version pname;

          configurePhase = ''
            runHook preConfigure
            mkdir std
            cp ${constant_va} std/constants.va
            cp ${disciplines_va} std/disciplines.va
          '';

          buildPhase = ''
            runHook preBuild
            mkdir -p $out
            verilogae prepare-build-dir --target ${target}  --module-name ${moduleName} --package-name ${pname} --std $PWD/std --version ${version}  --build-dir $out ${mainfile}
            runHook postBuild
          '';

          installPhase = ''
            runHook preInstall
            runHook postInstall
          '';

          nativeBuildInputs = [verilogae] ++ nativeBuildInputs;
    };
in
    rustPlatform.buildRustPackage   (
        args
        // {

            src = rustBuildDir;

            buildPhase = args.buildPhase or ''
                runHook preBuild
                (
                set -x
                env \
                  "CC_${rust.toRustTarget stdenv.buildPlatform}"="${ccForBuild}" \
                  "CXX_${rust.toRustTarget stdenv.buildPlatform}"="${cxxForBuild}" \
                  "CC_${rust.toRustTarget stdenv.hostPlatform}"="${ccForHost}" \
                  "CXX_${rust.toRustTarget stdenv.hostPlatform}"="${cxxForHost}" \
                  verilogae compile-build-dir --target ${target}  --build-dir $PWD --offline
                )
                runHook postBuild
           '';

            installPhase = ''
                runHook preInstall
                mkdir -p $out
                find . -name '*.whl' -exec mv {} $out \;
                runHook postInstall
            '';


            nativeBuildInputs = [verilogae] ++ nativeBuildInputs;

            doCheck = args.doCheck or false;
            inherit target name version pname;
        }
        # // Cross compilation is sadly broken
        # lib.optionalAttrs stdenv.hostPlatform.isMinGW {PYO3_CROSS_LIB_DIR="${./windows_python_lib}";}
    )



