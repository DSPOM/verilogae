//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use openvaf_diagnostics::{AnnotationType, DiagnosticSlice, FooterItem, LibraryDiagnostic, Text};
use openvaf_session::sourcemap::Span;
use thiserror::Error;

#[derive(Debug, Clone, Error)]
pub enum AttributeError {
    #[error("The 'retrive' attribute only accepts references to variables that shall be treated as parameters as input")]
    ExpectedVariableRefForExtract(Span),

    #[error("The 'op_var' attribute does not accept any value")]
    UnexpectedArgument(Span),

    #[error("The 'group' attribute only accepts string literals such as \"foo\"")]
    ExpectedStringLiteralForGroup(Span),
}

impl LibraryDiagnostic for AttributeError {
    fn annotation_type(&self) -> Option<AnnotationType> {
        Some(AnnotationType::Error)
    }

    fn slices(&self) -> Vec<DiagnosticSlice> {
        match self {
            Self::ExpectedVariableRefForExtract(span) => vec![DiagnosticSlice {
                slice_span: span.data(),
                messages: vec![(
                    AnnotationType::Error,
                    Text::const_str("Expected a constant value"),
                    span.data(),
                )],
                fold: false,
            }],

            Self::UnexpectedArgument(span) => vec![DiagnosticSlice {
                slice_span: span.data(),
                messages: vec![(
                    AnnotationType::Error,
                    Text::const_str("Unexpected argument!"),
                    span.data(),
                )],
                fold: false,
            }],
            Self::ExpectedStringLiteralForGroup(span) => vec![DiagnosticSlice {
                slice_span: span.data(),
                messages: vec![(
                    AnnotationType::Error,
                    Text::const_str("Expected a string literal"),
                    span.data(),
                )],
                fold: false,
            }],
        }
    }

    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem{
            id: None,
            label: Text::const_str("The retrive attribute accepts the following values:\n * No value ('retrive') \n * A variable reference (for example 'retrive = foo') \n * or an array of variable reference (for example ' retrive = '{foo, bar} ')"),
            annotation_type: AnnotationType::Help
        }]
    }
}
