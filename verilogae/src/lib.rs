//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use std::path::PathBuf;

use anyhow::{bail, Context, Result};
use openvaf_hir::lowering::lower_hir_userfacing_with_printer;
use openvaf_hir::AllowedOperations;
use openvaf_middle::functions::{CurrentLim, DefaultFunctions, ParameterCallType, VoltageLim};

pub use crate::functions::VariableFunction;
use crate::hir_lowering::{UserRequests, VariableFunctionFinder};
use clap::Clap;
pub use codegen::CodeGen;
use openvaf_ast_lowering::lower_ast_userfacing_with_printer;
use openvaf_data_structures::index_vec::{IndexSlice, IndexVec};
use openvaf_data_structures::HashMap;
use openvaf_diagnostics::lints::Linter;
use openvaf_diagnostics::{
    DiagnosticSlicePrinter, ExpansionPrinter, MultiDiagnostic, StandardPrinter, UserResult,
};
use openvaf_ir::ids::{CallArg, ModuleId};
use openvaf_ir::ids::{ParameterId, VariableId};
use openvaf_ir::ConstVal;
use openvaf_middle::cfg::ControlFlowGraph;
use openvaf_middle::const_fold::ConstantPropagation;
use openvaf_middle::derivatives::RValueAutoDiff;
use openvaf_middle::dfa::lattice::FlatSet;

use openvaf_middle::{COperand, CfgConversion, Mir, Operand, OperandData, RValue, StmntKind};
use openvaf_parser::parse_facing_with_printer;
use openvaf_pass::program_dependence::data_dependence::BuildUseDefGraph;
use openvaf_pass::{
    program_dependence::{control_dependence::BuildControlDependenceGraph, BuildPDG},
    FindAssignments, ReachingDefinitionsAnalysis, Simplify, SimplifyBranches, Verify,
};
use openvaf_preprocessor::preprocess_user_facing_with_printer;
use openvaf_session::sourcemap::{FileId, Span, StringLiteral};
use openvaf_session::symbols::Symbol;
use openvaf_session::Session;
use openvaf_session::SourceMap;
use rayon::prelude::*;
use target_lexicon::Triple;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::fmt::Layer;
use tracing_subscriber::layer::SubscriberExt;

use derive_more::{Display, From, TryInto};
use openvaf_macros::CfgFunctions;
use openvaf_middle::{
    functions::{CfgFunction, CfgFunctionEnum, CfgFunctions},
    inputs::DefaultInputs,
};

mod codegen;
mod errors;
mod functions;
mod hir_lowering;
mod lints;

pub struct OperatingPointVariable {
    pub name: Symbol,
    pub desc: StringLiteral,
    pub unit: StringLiteral,
}

impl OperatingPointVariable {
    pub fn new(var: VariableId, mir: &Mir) -> Self {
        let var = &mir.variables[var];
        Self {
            name: var.ident.name,
            desc: var.desc.unwrap_or(StringLiteral::DUMMY),
            unit: var.unit.unwrap_or(StringLiteral::DUMMY),
        }
    }
}

#[derive(Clap, Clone)]
pub struct CodeGenArgs {
    /// Specifies
    #[clap(long = "target-cpu", default_value = "generic")]
    pub cpu: String,
    #[clap(long = "target")]
    pub triple: Option<Triple>,
    #[clap(long)]
    pub debug: bool,
    #[clap(long = "target-features")]
    pub target_features: Option<Vec<String>>,
}

#[derive(Clone, Debug, Display, From, TryInto, CfgFunctions)]
#[cfg_inputs(DefaultInputs)]
pub enum VaeFuntions {}

struct FunctionMapper {}

impl CfgConversion<DefaultFunctions, VaeFuntions> for FunctionMapper {
    fn map_input(&mut self, src: DefaultInputs) -> OperandData<DefaultInputs> {
        src.into()
    }

    fn map_call_val(
        &mut self,
        call: DefaultFunctions,
        _args: IndexVec<CallArg, Operand>,
        span: Span,
    ) -> RValue<VaeFuntions> {
        match call {
            DefaultFunctions::Noise(_)
            | DefaultFunctions::TimeDerivative(_)
            | DefaultFunctions::AcStimulus(_) => {
                // TODO warn
                Operand {
                    span,
                    contents: OperandData::Constant(0.0.into()),
                }
                .into()
            }
            DefaultFunctions::Analysis(_) => {
                // TODO warn
                Operand {
                    span,
                    contents: OperandData::Constant(true.into()),
                }
                .into()
            }
            DefaultFunctions::VoltageLim(VoltageLim { voltage, .. }) => Operand {
                span,
                contents: OperandData::Read(voltage.into()),
            }
            .into(),
            DefaultFunctions::CurrentLim(CurrentLim { current, .. }) => Operand {
                span,
                contents: OperandData::Read(current.into()),
            }
            .into(),

            DefaultFunctions::StopTask(_)
            | DefaultFunctions::Print(_)
            | DefaultFunctions::NodeCollapse(_)
            | DefaultFunctions::Discontinuity(_) => unreachable!(),
        }
    }

    fn map_call_stmnt(
        &mut self,
        call: DefaultFunctions,
        _args: IndexVec<CallArg, Operand>,
        _span: Span,
    ) -> StmntKind<VaeFuntions> {
        match call {
            // TODO warn
            DefaultFunctions::StopTask(_)
            | DefaultFunctions::Print(_)
            | DefaultFunctions::NodeCollapse(_)
            | DefaultFunctions::Discontinuity(_) => StmntKind::NoOp,

            DefaultFunctions::Noise(_)
            | DefaultFunctions::TimeDerivative(_)
            | DefaultFunctions::VoltageLim(_)
            | DefaultFunctions::CurrentLim(_)
            | DefaultFunctions::AcStimulus(_)
            | DefaultFunctions::Analysis(_) => unreachable!(),
        }
    }
}

impl From<ParameterCallType> for VaeFuntions {
    fn from(no_op: ParameterCallType) -> Self {
        match no_op {}
    }
}

// #[derive(Copy, Clone, Debug, PartialEq)]
// pub enum VerilogAeUnimplimetedCall {
//     Noise,
//     TimeDerivative,
// }

// #[derive(Copy, Clone, Debug, PartialEq)]
// pub struct VerilogAeCall {
//     kind: VerilogAeUnimplimetedCall,
//     sctx: SyntaxCtx,
//     span: Span,
// }

// impl Display for VerilogAeCall {
//     fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
//         Debug::fmt(self, f)
//     }
// }

// impl CallType for VerilogAeCall {
//     type I = VerilogAeInput;

//     fn const_fold(&self, _call: &[FlatSet<ConstVal>]) -> FlatSet<ConstVal> {
//         FlatSet::Top
//     }

//     fn derivative<C: CallType>(
//         &self,
//         _args: &IndexSlice<CallArg, [COperand<Self>]>,
//         ad: &mut RValueAutoDiff<Self, C>,
//         span: Span,
//     ) -> Option<RValue<Self>> {
//         Some(RValue::Use(Spanned {
//             contents: OperandData::Copy(ad.original_local),
//             span,
//         }))
//     }
// }

pub fn init_env_logger() -> Result<()> {
    let filter = tracing_subscriber::EnvFilter::from_env("VERILOGAE_LOG")
        .add_directive(LevelFilter::INFO.into());
    let layer = Layer::new()
        .with_ansi(true)
        .compact()
        .without_time()
        .with_target(false);

    let subscriber = tracing_subscriber::Registry::default()
        .with(filter)
        .with(layer);

    tracing::subscriber::set_global_default(subscriber).context("Logging setup failed!")?;
    Ok(())
}

pub fn init() -> Result<Session> {
    let session = Session::new();
    init_env_logger()?;
    Ok(session)
}

pub fn run_frontend<P: DiagnosticSlicePrinter>(
    sm: Box<SourceMap>,
    main_file: FileId,
    paths: HashMap<&'static str, PathBuf>,
) -> UserResult<(Mir, UserRequests), P> {
    let ts = preprocess_user_facing_with_printer(sm, main_file, paths)?;
    let ast = parse_facing_with_printer(ts)?;
    let hir = lower_ast_userfacing_with_printer(ast, |_| AllowedOperations::all())?;
    let diagnostic = Linter::early_user_diagnostics()?;
    eprint!("{}", diagnostic);
    let mut lowering = VariableFunctionFinder::new(&hir);
    let mir = lower_hir_userfacing_with_printer(hir, &mut lowering)?;

    if lowering.errors.is_empty() {
        Ok((mir, lowering.dst))
    } else {
        Err(lowering.errors.user_facing())
    }
}

pub struct CodegenTasks {
    pub functions: Vec<VariableFunction>,
    pub op_vars: Vec<OperatingPointVariable>,
    pub parameter_groups: IndexVec<ParameterId, StringLiteral>,
}

pub const BUG_LINK: &str = "https://gitlab.com/DSPOM/verilogae/-/issues/new";

impl UserRequests {
    pub fn prepare_codegen(
        self,
        mir: &Mir,
        backtrace: bool,
        verify: bool,
        nothreads: bool,
    ) -> Result<CodegenTasks> {
        let module = if let Some(first) = mir.modules.first() {
            first
        } else {
            bail!("VerilogAE currently only supports single module VerilogA files")
        };
        let mut cfg = module
            .analog_cfg
            .borrow()
            .clone()
            .map(&mut FunctionMapper {});

        let mut errors = MultiDiagnostic(Vec::new());
        cfg.generate_derivatives(mir, &mut errors);
        if !errors.is_empty() {
            let err = if backtrace {
                errors.user_facing::<ExpansionPrinter>().into()
            } else {
                errors.user_facing::<StandardPrinter>().into()
            };
            return Err(err);
        }

        if verify {
            verify_cfg(&mut cfg, mir, "creation")?
        }

        cfg.insert_variable_declarations(mir);

        if verify {
            verify_cfg(&mut cfg, mir, "variable_initialization")?
        }

        cfg.modify(ConstantPropagation::default());

        if verify {
            verify_cfg(&mut cfg, mir, "constant propagation")?
        }

        cfg.modify(SimplifyBranches);
        cfg.modify(Simplify);

        if verify {
            verify_cfg(&mut cfg, mir, "cfg-simplification")?
        }

        let locations = cfg.intern_locations();
        let assignments = cfg.analyse(FindAssignments(&locations));
        let reaching_definitions = cfg.analyse(ReachingDefinitionsAnalysis {
            assignments: &assignments,
            locations: &locations,
        });
        let use_def_graph = cfg.analyse(BuildUseDefGraph {
            locations: &locations,
            assignments: &assignments,
            reaching_definitions: &reaching_definitions,
        });

        assert!(!cfg.blocks.is_empty(), "EMPTY CFG. VERY ILLEGAL");
        let mut reaching_definitions_cursor = reaching_definitions.as_results_cursor(&cfg);
        reaching_definitions_cursor.seek_to_exit_block_end(&cfg);
        let reachable_at_program_end = reaching_definitions_cursor.get().to_owned();

        let pdg = cfg.analyse(BuildPDG {
            locations: &locations,
            use_def_graph,
            assignments,
            control_dependence: BuildControlDependenceGraph::default(),
        });

        #[cfg(debug_assertions)]
        mir.print_to_file_with_shared(
            "full.mir",
            openvaf_ir::ids::ModuleId::from_raw_unchecked(0),
            &cfg,
        )
        .unwrap();

        let functions: Result<_> = if nothreads {
            self.functions
                .into_iter()
                .map(|attr| {
                    VariableFunction::new(
                        attr.output,
                        &attr.inputs,
                        &cfg,
                        &locations,
                        &pdg,
                        &reachable_at_program_end,
                        mir,
                    )
                })
                .collect()
        } else {
            self.functions
                .into_par_iter()
                .map(|attr| {
                    VariableFunction::new(
                        attr.output,
                        &attr.inputs,
                        &cfg,
                        &locations,
                        &pdg,
                        &reachable_at_program_end,
                        mir,
                    )
                })
                .collect()
        };

        let op_vars = self
            .op_vars
            .into_iter()
            .map(|var| OperatingPointVariable::new(var, mir))
            .collect();

        Ok(CodegenTasks {
            functions: functions?,
            op_vars,
            parameter_groups: self.groups,
        })
    }
}

fn verify_cfg(cfg: &ControlFlowGraph<VaeFuntions>, mir: &Mir, stage: &str) -> Result<()> {
    let malformations = cfg.analyse(Verify(mir));
    if !malformations.is_empty() {
        let malformations_file = format!("{}.log", stage);
        malformations.print_to_file(&malformations_file).unwrap();

        let invalid_mir_file = format!("{}_invalid.mir", stage);
        mir.print_to_file_with_shared(&invalid_mir_file, ModuleId::from_raw_unchecked(0), &cfg)
            .unwrap();
        bail!("VerilogAE produced an illegal MIR during {}! Please post a bug report containing this error message to {} and append the following files: {}, {}", stage, BUG_LINK, malformations_file, invalid_mir_file)
    }
    Ok(())
}
