//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use anyhow::{bail, Result};
use openvaf_data_structures::bit_set::{BitSet, SparseBitMatrix};
use openvaf_ir::ids::{BranchId, NodeId, ParameterId, PortId, VariableId};
use openvaf_middle::cfg::{ControlFlowGraph, IntLocation, InternedLocations, TerminatorKind};
use openvaf_middle::inputs::{
    CurrentProbe, DefaultInputs, ParameterInput, PortConnected, SimParam, SimParamKind, Voltage,
};
use openvaf_middle::{Local, LocalKind, Mir, OperandData, StmntKind, VariableLocalKind};
use openvaf_pass::{
    program_dependence::ProgramDependenceGraph, BackwardSlice, RemoveDeadLocals, Simplify, Strip,
};
use openvaf_session::sourcemap::StringLiteral;
use openvaf_session::symbols::Symbol;
use tracing::{debug, warn};

use crate::VaeFuntions;

#[derive(Clone)]
pub struct VariableFunction {
    pub cfg: ControlFlowGraph<VaeFuntions>,
    pub inputs: Vec<(Symbol, Local)>,
    pub output: Local,
    pub voltages: Vec<(NodeId, NodeId)>,
    pub currents: Vec<BranchId>,
    pub simparams: Vec<StringLiteral>,
    pub simparams_str: Vec<StringLiteral>,
    pub simparams_opt: Vec<StringLiteral>,
    pub ports_connected: Vec<PortId>,
    pub temperature: bool,
    pub parameters: Vec<ParameterId>,
    pub ffi_name: String,
    pub name: Symbol,
    pub description: StringLiteral,
    pub units: StringLiteral,
}

impl<'a> VariableFunction {
    pub fn new(
        output_var: VariableId,
        input_vars: &[VariableId],
        src_cfg: &'a ControlFlowGraph<VaeFuntions>,
        locations: &InternedLocations,
        pdg: &ProgramDependenceGraph<SparseBitMatrix<Local, IntLocation>>,
        reachable_at_program_end: &BitSet<IntLocation>,
        mir: &Mir,
    ) -> Result<Self> {
        let ffi_name = format!("__vae_function_{}", output_var);
        debug!(
            variable = display(mir[output_var].ident),
            ffi_name = ffi_name.as_str(),
            "Creating slice"
        );

        let mut cfg = src_cfg.clone();
        let mut output = None;
        let mut inputs = Vec::with_capacity(input_vars.len());
        for (local, decl) in cfg.locals.iter_enumerated() {
            match decl.kind {
                LocalKind::Variable(var, VariableLocalKind::User) if var == output_var => {
                    output = Some(local)
                }
                LocalKind::Variable(ref var, VariableLocalKind::User)
                    if input_vars.contains(var) =>
                {
                    inputs.push((*var, local))
                }
                _ => (),
            }
        }

        let output = if let Some(out) = output {
            out
        } else {
            // TODO error with span
            bail!(
                "Retrieved variable {} is never written to!",
                mir[output_var].ident.name
            )
        };

        let relevant_locations = cfg.analyse(
            BackwardSlice::new(pdg, locations)
                .assuming_locals(inputs.iter().map(|(_, local)| *local))
                .requiring_local_in(output, reachable_at_program_end),
        );

        cfg.modify(Strip {
            retain: &relevant_locations,
            locations,
        });

        cfg.modify(Simplify);

        debug!("after simplify before replace");

        #[cfg(debug_assertions)]
        mir.print_to_file_with_shared(
            format!("__vae_function_{}.mir", output_var),
            openvaf_ir::ids::ModuleId::from_raw_unchecked(0),
            &cfg,
        )
        .unwrap();

        let replacements = cfg.modify(RemoveDeadLocals);
        debug!("after replace");

        let output = replacements[output];
        let inputs: Vec<_> = inputs
            .into_iter()
            .filter_map(|(var, local)| {
                if replacements[local] < cfg.locals.len_idx(){
                    Some((mir[var].ident.name, replacements[local]))
                }else {
                    warn!("Variable '{}' marked for retrieval does not depend on variable '{}' marked as input", mir[output_var].ident,mir[var].ident);
                    None
                }
            })
            .collect();

        for (_, input) in &inputs {
            cfg.locals[*input].kind = LocalKind::Temporary
        }

        #[cfg(debug_assertions)]
        mir.print_to_file_with_shared(
            format!("__vae_function_{}.mir", output_var),
            openvaf_ir::ids::ModuleId::from_raw_unchecked(0),
            &cfg,
        )
        .unwrap();

        let mut res = Self {
            cfg,
            inputs,
            output,
            parameters: Vec::with_capacity(32),
            voltages: Vec::with_capacity(8),
            currents: Vec::with_capacity(8),
            simparams: Vec::new(),
            simparams_str: Vec::new(),
            simparams_opt: Vec::new(),
            ports_connected: Vec::new(),
            ffi_name,
            temperature: false,
            name: mir[output_var].ident.name,
            description: mir[output_var].desc.unwrap_or(StringLiteral::DUMMY),
            units: mir[output_var].unit.unwrap_or(StringLiteral::DUMMY),
        };

        res.find_dependencies();

        Ok(res)
    }

    fn find_dependencies(&mut self) {
        for block in self.cfg.blocks.iter() {
            let stmnt_operands = block.statements.iter().filter_map(|(stmnt, _)| {
                if let StmntKind::Assignment(_, val) = stmnt {
                    Some(val.operands())
                } else {
                    None
                }
            });

            let terminator_operator =
                if let TerminatorKind::Split { condition, .. } = &block.terminator().kind {
                    Some(condition.operands())
                } else {
                    None
                };

            for op in stmnt_operands.chain(terminator_operator).flatten() {
                if let OperandData::Read(ref input) = op.contents {
                    match input {
                        DefaultInputs::Parameter(ParameterInput::Value(param)) => {
                            if !self.parameters.contains(param) {
                                self.parameters.push(*param)
                            }
                        }

                        DefaultInputs::PortConnected(PortConnected(port)) => {
                            if !self.ports_connected.contains(port) {
                                self.ports_connected.push(*port)
                            }
                        }

                        DefaultInputs::SimParam(SimParam {
                            name,
                            kind: SimParamKind::Real,
                        }) => {
                            if !self.simparams.contains(name) {
                                self.simparams.push(*name)
                            }
                        }

                        DefaultInputs::SimParam(SimParam {
                            name,
                            kind: SimParamKind::RealOptional,
                        })
                        | DefaultInputs::SimParam(SimParam {
                            name,
                            kind: SimParamKind::RealOptionalGiven,
                        }) => {
                            if !self.simparams_opt.contains(name) {
                                self.simparams_opt.push(*name)
                            }
                        }

                        DefaultInputs::SimParam(SimParam {
                            name,
                            kind: SimParamKind::String,
                        }) => {
                            if !self.simparams_str.contains(name) {
                                self.simparams_str.push(*name)
                            }
                        }

                        DefaultInputs::CurrentProbe(CurrentProbe(branch)) => {
                            if !self.currents.contains(branch) {
                                self.currents.push(*branch)
                            }
                        }

                        DefaultInputs::Voltage(Voltage { hi, lo }) => {
                            if !self
                                .voltages
                                .iter()
                                .any(|voltage| matches!(voltage,(x,y)|(y,x) if x == hi && y == lo))
                            {
                                self.voltages.push((*hi, *lo))
                            }
                        }
                        DefaultInputs::Temperature(_) => self.temperature = true,
                        DefaultInputs::PartialTimeDerivative(_)
                        | DefaultInputs::Parameter(ParameterInput::Given(_)) => (),
                    }
                }
            }
        }
    }
}
