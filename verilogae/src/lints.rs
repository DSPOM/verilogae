//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use openvaf_diagnostics::declare_plugin_lint;
use openvaf_diagnostics::lints::{Lint, LintDiagnostic};
use openvaf_diagnostics::{AnnotationType, DiagnosticSlice, FooterItem, Text};
use openvaf_session::sourcemap::Span;
use std::error::Error;
use std::fmt::{Display, Formatter};

declare_plugin_lint!(noise, Deny);


#[derive(Debug)]
pub struct Noise {
    pub span: Span,
}
impl Display for Noise {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("VerilogAE does not support noise expressions inside retrieved code")
    }
}

impl Error for Noise {}

impl LintDiagnostic for Noise {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *noise
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        let label = if main_annotation_type == AnnotationType::Error {
            Text::const_str("Noise is not allowed")
        } else {
            Text::const_str("Noise is ignored (0.0 is used)")
        };

        vec![DiagnosticSlice {
            slice_span: self.span.data(),
            messages: vec![(main_annotation_type, label, self.span.data())],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::const_str(
                "When this lint is set to allow or warn this expression is evaluated as '0.0'",
            ),
            annotation_type: AnnotationType::Info,
        }]
    }
}

declare_plugin_lint!(port_connected, Deny);

#[derive(Debug)]
pub struct PortConnected {
    pub span: Span,
}
impl Display for PortConnected {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("VerilogAE does currently not properly support $port_connected")
    }
}

impl Error for PortConnected {}

impl LintDiagnostic for PortConnected {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *port_connected
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        let label = if main_annotation_type == AnnotationType::Error {
            Text::const_str("$port_connected is not allowed")
        } else {
            Text::const_str("$port_connected is ignored (1.0 is used)")
        };

        vec![DiagnosticSlice {
            slice_span: self.span.data(),
            messages: vec![(main_annotation_type, label, self.span.data())],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::const_str(
                "When this lint is set to allow or warn this expression is evaluated as tough the port is connected",
            ),
            annotation_type: AnnotationType::Info,
        }]
    }
}

declare_plugin_lint!(time_derivatives, Deny);

#[derive(Debug)]
pub struct TimeDerivative {
    pub span: Span,
}
impl Display for TimeDerivative {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(
            "VerilogAE does currently not impliment time derivatives (always assuming zero)",
        )
    }
}

impl Error for TimeDerivative {}

impl LintDiagnostic for TimeDerivative {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *time_derivatives
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        vec![DiagnosticSlice {
            slice_span: self.span.data(),
            messages: vec![(
                main_annotation_type,
                Text::const_str("Time derivative is required"),
                self.span.data(),
            )],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::const_str(
                "When this lint is set to allow or warn the time deriative is assumed to be 0.0",
            ),
            annotation_type: AnnotationType::Info,
        }]
    }
}

declare_plugin_lint!(stop_tasks, Deny);

#[derive(Debug)]
pub struct StopTasksUnsupported {
    pub span: Span,
}
impl Display for StopTasksUnsupported {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("VerilogAE does currently not properly support $stop and $finish")
    }
}

impl Error for StopTasksUnsupported {}

impl LintDiagnostic for StopTasksUnsupported {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *stop_tasks
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        let label = if main_annotation_type == AnnotationType::Error {
            Text::const_str("Not supported")
        } else {
            Text::const_str("This will panic at runtime!")
        };

        vec![DiagnosticSlice {
            slice_span: self.span.data(),
            messages: vec![(main_annotation_type, label, self.span.data())],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::const_str(
                "When this lint is set to allow or warn this statement will panic causing an error",
            ),
            annotation_type: AnnotationType::Info,
        }]
    }
}

declare_plugin_lint!(non_constant_simparameter, Deny);

#[derive(Debug)]
pub enum SimParamDefault {
    RealOptional,
    Real,
    String,
}

impl Display for SimParamDefault {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            SimParamDefault::RealOptional => "The default value",
            SimParamDefault::Real => "0.0",
            SimParamDefault::String => "A dummy string (\"\")",
        };
        f.write_str(message)
    }
}

#[derive(Debug)]
pub struct NonConstantSimParam(pub Span, pub SimParamDefault);

impl Display for NonConstantSimParam {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("VerilogAe doesn't support calls to $simparam with non constant strings")
    }
}

impl Error for NonConstantSimParam {}

impl LintDiagnostic for NonConstantSimParam {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *non_constant_simparameter
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        let label = Text::const_str("Expected a constant string");
        vec![DiagnosticSlice {
            slice_span: self.0.data(),
            messages: vec![(main_annotation_type, label, self.0.data())],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::owned(format!(
                "When this lint is set to allow or warn {} will be used",
                self.1
            )),
            annotation_type: AnnotationType::Info,
        }]
    }
}

declare_plugin_lint!(empty_group_name, Warn);

#[derive(Debug)]
pub struct EmptyGroupName(pub Span);

impl Display for EmptyGroupName {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("No value was supplied to the group attribute and as such it has no effect")
    }
}

impl Error for EmptyGroupName {}

impl LintDiagnostic for EmptyGroupName {
    #[allow(clippy::inline_always)]
    #[inline(always)]
    fn lint(&self) -> Lint {
        *empty_group_name
    }

    fn slices(&self, main_annotation_type: AnnotationType) -> Vec<DiagnosticSlice> {
        vec![DiagnosticSlice {
            slice_span: self.0.data(),
            messages: vec![(
                main_annotation_type,
                Text::const_str("Useless group attribute"),
                self.0.data(),
            )],
            fold: false,
        }]
    }

    #[allow(clippy::inline_always)]
    #[inline]
    fn footer(&self) -> Vec<FooterItem> {
        vec![FooterItem {
            id: None,
            label: Text::const_str("Add a group using group=\"group_name\""),
            annotation_type: AnnotationType::Help,
        }]
    }
}
