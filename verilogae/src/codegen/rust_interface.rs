//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::{functions::VariableFunction, OperatingPointVariable};
use openvaf_ir::Type;
use openvaf_middle::{Mir, SimpleType};
use openvaf_session::with_sourcemap;
use proc_macro2::{Ident, Span, TokenStream};
use quote::{format_ident, quote, ToTokens};
use std::iter::repeat;
use std::ops::Deref;

#[derive(Copy, Clone)]
pub struct SimpleFFiTy(SimpleType);
impl ToTokens for SimpleFFiTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            SimpleType::Real => quote!(f64),
            SimpleType::String => quote!(*const c_char),
            SimpleType::Bool => quote!(bool),
            SimpleType::Integer => quote!(i64),
            SimpleType::Cmplx => unreachable!(),
        }
        .to_tokens(tokens);
    }
}

#[derive(Copy, Clone)]
pub struct SimplePythonTy(SimpleType);
impl ToTokens for SimplePythonTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0 {
            SimpleType::Real => quote!(f64),
            SimpleType::String => quote!(PyFfiString),
            SimpleType::Bool => quote!(bool),
            SimpleType::Integer => quote!(i64),
            SimpleType::Cmplx => unreachable!(),
        }
        .to_tokens(tokens);
    }
}

pub struct FfiTy(pub Type);

impl ToTokens for FfiTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.0.with_info(|info| match info.dimensions.as_slice() {
            [] => SimpleFFiTy(info.element).to_tokens(tokens),
            [ref dimensions @ .., _] => {
                let elements = ArrayTy(dimensions, SimpleFFiTy(info.element));
                quote!(*const #elements).to_tokens(tokens)
            }
        })
    }
}

pub struct PythonTy(pub Type);

impl ToTokens for PythonTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        self.0.with_info(|info| match info.dimensions.as_slice() {
            [] => SimplePythonTy(info.element).to_tokens(tokens),
            [ref dimensions @ .., _] => {
                let elements = ArrayTy(dimensions, SimplePythonTy(info.element));
                quote!(PyFfiPtr<#elements>).to_tokens(tokens)
            }
        })
    }
}

pub struct ArrayTy<'a, E>(&'a [u32], E);

impl<'a, E: ToTokens + Copy> ToTokens for ArrayTy<'a, E> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        if let Some(len) = self.0.last() {
            let element = ArrayTy(&self.0[..self.0.len() - 1], self.1);
            quote!([#element; #len]).to_tokens(tokens)
        } else {
            self.1.to_tokens(tokens)
        }
    }
}

#[derive(Copy, Clone)]
pub struct RealArg;

impl ToTokens for RealArg {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        quote!(_: f64,).to_tokens(tokens)
    }
}

#[derive(Copy, Clone)]
pub struct BoolArg;

impl ToTokens for BoolArg {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        quote!(_: bool,).to_tokens(tokens)
    }
}

#[derive(Copy, Clone)]
pub struct StringArg;

impl ToTokens for StringArg {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        quote!(_: *const c_char,).to_tokens(tokens)
    }
}

pub struct VariableFunctionDefinition<'a>(pub &'a Mir, pub &'a VariableFunction);

impl<'a> ToTokens for VariableFunctionDefinition<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let VariableFunction {
            cfg,
            inputs,
            voltages,
            currents,
            simparams,
            simparams_str,
            simparams_opt,
            ports_connected,
            temperature,
            parameters,
            ffi_name: name,
            output,
            ..
        } = self.1;

        let name = Ident::new(name, Span::call_site());
        let temperature = if *temperature { Some(RealArg) } else { None };
        let parameters = parameters.iter().map(|param| FfiTy(self.0[*param].ty));
        let inputs = inputs.iter().map(|(_, local)| FfiTy(cfg.locals[*local].ty));
        let voltages = repeat(RealArg).take(voltages.len());
        let currents = repeat(RealArg).take(currents.len());
        let simparams = repeat(RealArg).take(simparams.len() + simparams_opt.len());
        let simparams_str = repeat(StringArg).take(simparams_str.len());
        let simparams_given = repeat(BoolArg).take(simparams_opt.len());
        let ports_connected = repeat(BoolArg).take(ports_connected.len());

        let res_ty = FfiTy(cfg.locals[*output].ty);

        quote! (
           fn #name(
               #(#voltages)*
               #(#currents)*
               #(#simparams)*
               #(#simparams_str)*
               #(#simparams_given)*
               #(#ports_connected)*
               #temperature
               #(_:#parameters,)*
               #(_:#inputs),*
           ) -> #res_ty;
        )
        .to_tokens(tokens)
    }
}

struct Access<T: Into<usize> + Copy> {
    name: &'static str,
    idx: T,
    numeric: bool,
}

impl<T: Into<usize> + Copy> ToTokens for Access<T> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let idx: usize = self.idx.into();
        let name = format_ident!("{}_{}", self.name, idx);
        if self.numeric {
            quote!(#name.get_unchecked(i)).to_tokens(tokens)
        } else {
            quote!(#name.into_ffi()).to_tokens(tokens)
        }
    }
}

struct ValList<I: Iterator> {
    name: &'static str,
    arr: I,
}

impl<T: Into<usize> + Copy, I: Iterator<Item = (T, Type)> + Clone> ToTokens for ValList<I> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let names = self.arr.clone().map(|(idx, ty)| Access {
            name: self.name,
            idx,
            numeric: matches!(ty, Type::REAL | Type::INT),
        });
        quote!(#(#names,)*).to_tokens(tokens)
    }
}

pub struct VariableFunctionCall<'a>(pub &'a VariableFunction, pub &'a Mir);

impl<'a> ToTokens for VariableFunctionCall<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let VariableFunction {
            cfg,
            inputs,
            voltages,
            currents,
            simparams,
            simparams_str,
            simparams_opt,
            ports_connected,
            temperature,
            parameters,
            ffi_name: name,
            ..
        } = self.0;

        let name = Ident::new(name, Span::call_site());

        let voltages = ValList {
            name: "pot",
            arr: (0..voltages.len()).map(|i| (i, Type::REAL)),
        };
        let currents = ValList {
            name: "flow",
            arr: currents.iter().map(|lit| (*lit, Type::REAL)),
        };
        let simparams = ValList {
            name: "simpara",
            arr: simparams
                .iter()
                .chain(simparams_opt)
                .map(|lit| (*lit, Type::REAL)),
        };
        let simparams_str = ValList {
            name: "simpara_str",
            arr: simparams_str.iter().map(|lit| (*lit, Type::STRING)),
        };
        let simparams_given = ValList {
            name: "simpara_given",
            arr: simparams_opt.iter().map(|lit| (*lit, Type::BOOL)),
        };
        let ports_connected = ValList {
            name: "port_connected",
            arr: ports_connected.iter().map(|port| (*port, Type::BOOL)),
        };

        let parameters = ValList {
            name: "param",
            arr: parameters.iter().map(|param| (*param, self.1[*param].ty)),
        };
        let inputs = ValList {
            name: "input",
            arr: inputs
                .iter()
                .map(|(_, local)| (*local, cfg.locals[*local].ty)),
        };
        let temperature = if *temperature {
            Some(quote!(temp.get_unchecked(i),))
        } else {
            None
        };

        quote! (
            #name(
               #voltages
               #currents
               #simparams
               #simparams_str
               #simparams_given
               #ports_connected
               #temperature
               #parameters
               #inputs
           )
        )
        .to_tokens(tokens)
    }
}

impl ToTokens for OperatingPointVariable {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self { name, desc, unit } = self;
        let _name = name.as_str();
        let name: &str = _name.deref();
        with_sourcemap(|sm| {
            let unit = unit.raw_contents(sm);
            let description = desc.raw_contents(sm);
            quote!(
                dict.set_item(#name,PyCell::new(py,OpVar{
                    name: #name,
                    description: #description,
                    unit: #unit,
                })?)?;
            )
            .to_tokens(tokens)
        })
    }
}
