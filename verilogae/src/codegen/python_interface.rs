//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::codegen::rust_interface::{FfiTy, PythonTy, VariableFunctionCall};
use crate::functions::VariableFunction;
use openvaf_ir::Type;
use openvaf_middle::Mir;
use openvaf_session::sourcemap::StringLiteral;
use openvaf_session::symbols::Symbol;
use openvaf_session::with_sourcemap;
use proc_macro2::TokenStream;
use proc_macro2::{Ident, Span};
use quote::{format_ident, quote, ToTokens};
use std::ops::Deref;

struct PythonArgRead<'a, N: Deref<Target = str>> {
    dst: Ident,
    src: &'a str,
    name: N,
    ty: Type,
}
impl<'a, N: Deref<Target = str>> ToTokens for PythonArgRead<'a, N> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self { dst, src, name, ty } = self;
        let name = name.deref();
        let src_ident = Ident::new(src, Span::call_site());
        let rust_ty = PythonTy(*ty);

        if matches!(*ty, Type::REAL | Type::INT) {
            quote! (
                 let #dst = Vectorized::from_dict(#src_ident,#name, #src, &mut iterations)?;
            )
            .to_tokens(tokens);
        } else {
            // Numpy only supports numeric arrays

            let error = format!("eval: Required argument '{}' is missing from {}", name, src);

            quote!(
                let #dst : #rust_ty = if let Some(kwargs) = kwargs{
                    if let Some(val) = kwargs.get_item(#name){
                        val.extract()?
                    }else{
                        return Err(pyo3::exceptions::PyTypeError::new_err(#error))
                    }
                }else{
                    return Err(pyo3::exceptions::PyTypeError::new_err(#error))
                };
            )
            .to_tokens(tokens);
        }
    }
}

struct OptSimPara(StringLiteral);

impl ToTokens for OptSimPara {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let dst = format_ident!("simpara_{}", self.0.index());
        let dst_given = format_ident!("simpara_given_{}", self.0.index());

        let name = self.0.unescaped_contents();

        quote! (
            let mut #dst_given = false;
            let #dst : Vectorized<f64> = if let Some(kwargs) = simparameters {
                if let Some(val) = kwargs.get_item(#name){
                    #dst_given= true;
                    Vectorized::from_python(val,#name,&mut iterations)?
                }else{
                    Vectorized::Scalar(0.0)
                }
            }else{
                Vectorized::Scalar(0.0)
            };
        )
        .to_tokens(tokens);
    }
}

// We only allow defaults for real values (0.0) everything else must be provided
struct RealPythonArgRead<'a, T: Deref<Target = str>> {
    dst: Ident,
    src: &'a str,
    name: T,
    opt: bool,
}
impl<'a, T: Deref<Target = str>> ToTokens for RealPythonArgRead<'a, T> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let Self {
            dst,
            name,
            opt,
            src,
        } = self;

        let src_ident = Ident::new(src, Span::call_site());
        let name = name.deref();

        if *opt {
            quote! (let #dst = Vectorized::from_dict_opt(#src_ident,#name,&mut iterations)?;)
                .to_tokens(tokens);
        } else {
            quote! (
                 let #dst = Vectorized::from_dict(#src_ident,#name, #src, &mut iterations)?;
            )
            .to_tokens(tokens);
        }
    }
}

struct VariableEvalFunction<'a>(&'a VariableFunction, &'a Mir);

impl<'a> ToTokens for VariableEvalFunction<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let VariableFunction {
            cfg,
            inputs,
            output,
            voltages,
            currents,
            simparams,
            simparams_str,
            simparams_opt,
            ports_connected,
            temperature,
            parameters,
            ..
        } = self.0;

        let read_input = inputs.iter().map(|(name, local)| PythonArgRead {
            dst: format_ident!("input_{}", local.index()),
            src: "kwargs",
            name: name.as_str(),
            ty: cfg.locals[*local].ty,
        });

        let read_paras = parameters.iter().map(|param| PythonArgRead {
            dst: format_ident!("param_{}", param.index()),
            src: "kwargs",
            name: self.1[*param].ident.name.as_str(),
            ty: self.1[*param].ty,
        });

        let read_voltages = voltages
            .iter()
            .enumerate()
            .map(|(idx, (hi, lo))| RealPythonArgRead {
                dst: format_ident!("pot_{}", idx),
                src: "voltages",
                name: format!("br_{}{}", self.1[*hi].ident, self.1[*lo].ident),
                opt: true,
            });

        let read_currents = currents.iter().map(|branch| RealPythonArgRead {
            dst: format_ident!("flow_{}", branch.index()),
            src: "currents",
            name: self.1[*branch].ident.name.as_str(),
            opt: true,
        });

        let read_temperature = if *temperature {
            Some(PythonArgRead {
                dst: Ident::new("temp", Span::call_site()),
                src: "kwargs",
                name: "temperature",
                ty: Type::REAL,
            })
        } else {
            None
        };

        let read_simparas = simparams.iter().map(|param| PythonArgRead {
            dst: format_ident!("simpara_{}", param.index()),
            src: "simparameters",
            name: param.unescaped_contents(),
            ty: Type::REAL,
        });

        let read_opt_simparas = simparams_opt.iter().map(|&param| OptSimPara(param));

        let read_simparas_str = simparams_str.iter().map(|param| PythonArgRead {
            dst: format_ident!("simpara_str_{}", param.index()),
            src: "simparameters",
            name: param.unescaped_contents(),
            ty: Type::REAL,
        });

        // TODO ports connected python interface
        let read_ports_connected = ports_connected.iter().map(|port| {
            let ident = format_ident!("port_connected_{}", port.index());
            quote!(let #ident = true;)
        });

        let res_ty = cfg.locals[*output].ty;

        let invocation = CompiledCodeInvocation {
            func: self.0,
            mir: self.1,
            numeric: matches!(res_ty, Type::REAL | Type::INT),
        };

        let res_ty = match res_ty {
            Type::REAL => ReturnTy::RealArray,
            Type::INT => ReturnTy::IntArray,
            ty => ReturnTy::Other(ty),
        };

        quote!(
                 /// Calulates the extracted variables from parameters, in-variables and branches specified in kwargs
                /// Requires the temperature and all branches that need both flow and potential
                /// Branches that only require potential or flow default to 0
                /// Parameters and input variables default to default values from the verilog file.
                #[staticmethod]
                #[args("*",voltages = "None" ,currents = "None",kwargs = "**")]
                #[text_signature = "(/,temperature,voltages={},currents={},simparameters={},**modelcard)"]
                pub fn eval(voltages:Option<&PyDict>, currents:Option<&PyDict>, simparameters:Option<&PyDict>, kwargs:Option<&PyDict>,py:Python) -> PyResult<#res_ty>{
                    let mut iterations = 1;
                    #(#read_input)*
                    #(#read_paras)*
                    #(#read_voltages)*
                    #(#read_currents)*
                    #read_temperature
                    #(#read_simparas)*
                    #(#read_opt_simparas)*
                    #(#read_simparas_str)*
                    #(#read_ports_connected)*
                    #invocation

                }
            ).to_tokens(tokens)
    }
}

enum ReturnTy {
    IntArray,
    RealArray,
    Other(Type),
}

impl ToTokens for ReturnTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            ReturnTy::IntArray => quote!(Py<PyArray1<i64>>).to_tokens(tokens),
            ReturnTy::RealArray => quote!(Py<PyArray1<f64>>).to_tokens(tokens),
            ReturnTy::Other(ty) => FfiTy(*ty).to_tokens(tokens),
        }
    }
}

struct CompiledCodeInvocation<'a> {
    func: &'a VariableFunction,
    mir: &'a Mir,
    numeric: bool,
}

impl<'a> ToTokens for CompiledCodeInvocation<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let call = VariableFunctionCall(self.func, self.mir);
        if self.numeric {
            quote!(
                let mut res = Vec::with_capacity(iterations);
                py.allow_threads(
                    || (0..iterations).into_par_iter().map(|i|unsafe {
                        #call
                    }).collect_into_vec(&mut res)
                );
                Ok(PyArray1::from_vec(py,res).to_owned())
            )
            .to_tokens(tokens)
        } else {
            quote!(
                if iterations != 1{
                    return return pyo3::exceptions::TypeError::into("Numpy vectored calls are only supported for numeric variables (real or integer)")
                }
                let i = 0;
                let res = #call
                Ok(res)
            ).to_tokens(tokens);
        }
    }
}

struct SymbolAsStr(Symbol);
impl ToTokens for SymbolAsStr {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let name = self.0.as_str();
        let res = name.deref();
        quote!(#res).to_tokens(tokens)
    }
}

enum PyList<T: ToTokens, I: Iterator<Item = T>> {
    Empty,
    Filled(I),
}

impl<T: ToTokens, I: Iterator<Item = T> + Clone> ToTokens for PyList<T, I> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self {
            Self::Empty => quote!(PyList::empty(py)).to_tokens(tokens),
            Self::Filled(contents) => {
                let contents = contents.clone();
                quote!(PyList::new(py,[#(#contents),*].iter())).to_tokens(tokens)
            }
        }
    }
}

pub struct VariableFunctionPythonClass<'a>(pub &'a VariableFunction, pub &'a Mir);

impl<'a> ToTokens for VariableFunctionPythonClass<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let VariableFunction {
            voltages,
            currents,
            simparams,
            simparams_str,
            simparams_opt,
            temperature,
            parameters,
            ffi_name,
            name,
            description,
            units,
            ..
        } = self.0;
        let eval_method = VariableEvalFunction(self.0, self.1);
        let name = &*name.as_str();
        let ffi_name = Ident::new(&format!("{}_class", ffi_name), Span::call_site());

        let voltages =
            if voltages.is_empty() {
                PyList::Empty
            } else {
                PyList::Filled(voltages.iter().map(|(hi, lo)| {
                    format!("br_{}{}", self.1[*hi].ident.name, self.1[*lo].ident.name)
                }))
            };

        let currents = if currents.is_empty() {
            PyList::Empty
        } else {
            PyList::Filled(
                currents
                    .iter()
                    .map(|branch| SymbolAsStr(self.1[*branch].ident.name)),
            )
        };

        let parameters = if parameters.is_empty() {
            PyList::Empty
        } else {
            PyList::Filled(
                parameters
                    .iter()
                    .map(|param| SymbolAsStr(self.1[*param].ident.name)),
            )
        };

        let simparas = if (simparams.len() + simparams_str.len() + simparams_opt.len()) == 0 {
            PyList::Empty
        } else {
            PyList::Filled(
                simparams
                    .iter()
                    .map(|x| x.unescaped_contents())
                    .chain(simparams_str.iter().map(|x| x.unescaped_contents()))
                    .chain(simparams_opt.iter().map(|x| x.unescaped_contents())),
            )
        };

        with_sourcemap(|sm| {
            let description = description.raw_contents(sm);
            let unit = units.raw_contents(sm);

            quote! (
                #[pyclass]
                pub struct #ffi_name{}

                #[pymethods]
                impl #ffi_name{
                    #[classattr]
                    const name: &'static str = #name;
                    #[classattr]
                    const description: &'static str = #description;
                    #[classattr]
                    const unit: &'static str = #unit;

                    #[classattr]
                    const temperature_dependent: bool = #temperature;

                    /// All simparameters necessary to calculate this extraction
                    #[getter]
                    pub fn simparameters<'py>(&self,py:Python<'py>) ->  &'py PyList{
                        #simparas
                    }

                    /// All branch potentials necessary to calculate this extraction
                    #[getter]
                    pub fn voltages<'py>(&self,py:Python<'py>) ->  &'py PyList{
                        #voltages
                    }

                    /// All branch and port flows necessary to calculate this extraction
                    #[getter]
                    pub fn currents<'py>(&self,py:Python<'py>) ->  &'py PyList{
                        #currents
                    }

                    /// All parameters necessary to calculate this extraction
                    #[getter]
                    pub fn parameters<'py>(&self,py:Python<'py>) ->  &'py PyList{
                        #parameters
                    }
                    #eval_method
                }
            )
            .to_tokens(tokens)
        })
    }
}
