//  * ******************************************************************************************
//  * Copyright (c) 2021 Pascal Kuthe. This file is part of the VerilogAE project.
//  * It is subject to the license terms in the LICENSE file found in the top-level directory
//  *  of this distribution and at  https://gitlab.com/DSPOM/verilogae/blob/master/LICENSE.
//  *  No part of verilog-ae, including this file, may be copied, modified, propagated, or
//  *  distributed except according to the terms contained in the LICENSE file.
//  * *******************************************************************************************

use crate::functions::VariableFunction;
use crate::{CodeGenArgs, VaeFuntions};
use anyhow::{bail, Result};
use openvaf_codegen_llvm::inkwell::context::Context;
use openvaf_codegen_llvm::inkwell::module::Linkage;
use openvaf_codegen_llvm::inkwell::passes::{PassManager, PassManagerBuilder};
use openvaf_codegen_llvm::inkwell::support::LLVMString;
use openvaf_codegen_llvm::inkwell::targets::{
    CodeModel, FileType, InitializationConfig, RelocMode, Target, TargetMachine, TargetTriple,
};
use openvaf_codegen_llvm::inkwell::types::{BasicType, BasicTypeEnum};
use openvaf_codegen_llvm::inkwell::values::{BasicValue, BasicValueEnum};
use openvaf_codegen_llvm::inkwell::{AddressSpace, OptimizationLevel};
use openvaf_codegen_llvm::{CallTypeCodeGen, CfgCodegen, Intrinsic, LlvmCodegen, LocalValue};
use openvaf_data_structures::index_vec::IndexSlice;
use openvaf_ir::ids::CallArg;
use openvaf_middle::cfg::START_BLOCK;
use openvaf_middle::functions::DefaultFunctions;
use openvaf_middle::inputs::{
    CurrentProbe, DefaultInputs, ParameterInput, PortConnected, SimParam, SimParamKind, Voltage,
};
use openvaf_middle::{CfgFunctions, Mir};
use std::iter::repeat;
use std::ops::Range;
use std::path::{Path, PathBuf};
use tracing::debug;

impl CodeGenArgs {
    fn triple(&self) -> TargetTriple {
        if let Some(triple) = &self.triple {
            TargetTriple::create(&triple.to_string())
        } else {
            TargetMachine::get_default_triple()
        }
    }

    pub fn features(&self) -> String {
        let mut dst = String::new();
        if let Some(features) = &self.target_features {
            let mut iter = features.iter();

            if let Some(first) = iter.next() {
                dst.push_str(&first);
                for feature in iter {
                    dst.push(',');
                    dst.push_str(&feature);
                }
            }
        }

        dst
    }
}

pub(super) struct CodeGenCfg {
    msvc: bool,
    opt_lvl: OptimizationLevel,
    features: String,
    triple: String,
    cpu: String,
}

impl CodeGenCfg {
    pub fn new(args: &CodeGenArgs) -> Result<Self> {
        Target::initialize_all(&InitializationConfig::default());
        let opt_lvl = if args.debug {
            OptimizationLevel::None
        } else {
            OptimizationLevel::Aggressive
        };

        let triple = args.triple();
        let features = args.features();

        let triple_str = triple.as_str().to_bytes();
        let msvc = &triple_str[triple_str.len() - 4..] == b"msvc";

        let target = Target::from_triple(&triple).unwrap();
        if target
            .create_target_machine(
                &triple,
                &args.cpu,
                &features,
                opt_lvl,
                RelocMode::PIC,
                CodeModel::Default,
            )
            .is_none()
        {
            let triple = triple.as_str().to_string_lossy();
            bail!(
                "Target {} with cpu {} and features [{}] is not supported!",
                triple,
                &args.cpu,
                features
            )
        }

        Ok(Self {
            msvc,
            opt_lvl,
            features,
            cpu: args.cpu.clone(),
            triple: triple.as_str().to_str().unwrap().to_owned(),
        })
    }

    pub fn pass_manager_builder(&self) -> PassManagerBuilder {
        let builder = PassManagerBuilder::create();
        builder.set_optimization_level(self.opt_lvl);
        builder
    }

    pub fn target_machine(&self) -> TargetMachine {
        // TODO find out if this not save to just throw into a sync/send wrapper
        let triple = TargetTriple::create(&self.triple);
        let target = Target::from_triple(&triple).unwrap();
        target
            .create_target_machine(
                &TargetTriple::create(&self.triple),
                &self.cpu,
                &self.features,
                self.opt_lvl,
                RelocMode::PIC,
                CodeModel::Default,
            )
            .unwrap()
    }
}

impl VariableFunction {
    pub(super) fn gen_object(
        &self,
        opt: &CodeGenCfg,
        mir: &Mir,
        dst: &Path,
        verify: bool,
    ) -> Result<PathBuf, LLVMString> {
        let _span = debug!(
            file = display(&self.ffi_name),
            name = display(self.name),
            "compiling object"
        );

        let ctx = Context::create();
        let builder = opt.pass_manager_builder();
        let target = opt.target_machine();

        let target_data = target.get_target_data();
        let mut ctx = LlvmCodegen::new(mir, &ctx, opt.msvc, &target_data, &self.name.as_str());

        let mut cg = self.create_codegenerator(&mut ctx);

        cg.build_blocks();
        cg.ctx.builder.position_at_end(cg.blocks[self.cfg.end()]);
        let return_val = cg.read_local(self.output);

        cg.ctx.builder.build_return(Some(&return_val));

        let mut name = self.ffi_name.clone();
        name.push_str(".o");
        let path = dst.join(&name);

        if verify {
            if let Err(err) = cg.ctx.module.verify() {
                let mut name = self.ffi_name.clone();
                name.push_str(".ll");
                cg.ctx.module.print_to_file(&name)?;
                return Err(err);
            }
        }

        #[cfg(debug_assertions)]
        {
            let mut name = self.ffi_name.clone();
            name.push_str(".ll");
            cg.ctx.module.print_to_file(&name)?;
        }

        let fpm = PassManager::create(&cg.ctx.module);
        builder.populate_function_pass_manager(&fpm);
        fpm.run_on(&cg.function);

        let mpm = PassManager::create(());
        builder.populate_module_pass_manager(&mpm);
        mpm.run_on(&cg.ctx.module);

        #[cfg(debug_assertions)]
        {
            let mut name = self.ffi_name.clone();
            name.push_str(".ll");
            cg.ctx.module.print_to_file(&name)?;
        }

        if verify {
            if let Err(err) = cg.ctx.module.verify() {
                let mut name = self.ffi_name.clone();
                name.push_str(".ll");
                cg.ctx.module.print_to_file(&name)?;
                return Err(err);
            }
        }

        target.write_to_file(&cg.ctx.module, FileType::Object, &path)?;

        cg.local_values.clear();

        Ok(path)
    }

    fn create_codegenerator<'lt, 'a, 'c>(
        &'lt self,
        ctx: &'lt mut LlvmCodegen<'a, 'c, DefaultFunctions>,
    ) -> CfgCodegen<'lt, 'a, 'c, ExtractionCodeGen<'lt, 'c>, DefaultFunctions, VaeFuntions> {
        fn add_homogenous_args<'c>(
            list_len: usize,
            ty: BasicTypeEnum<'c>,
            args: &mut Vec<BasicTypeEnum<'c>>,
        ) -> Range<usize> {
            let res = args.len()..list_len + args.len();
            args.extend(repeat(ty).take(list_len));
            res
        }

        let mut args = Vec::with_capacity(32);
        let voltages = add_homogenous_args(
            self.voltages.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let currents = add_homogenous_args(
            self.currents.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams = add_homogenous_args(
            self.simparams.len() + self.simparams_opt.len(),
            ctx.real_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams_str = add_homogenous_args(
            self.simparams_str.len(),
            ctx.string_ty().as_basic_type_enum(),
            &mut args,
        );
        let simparams_given = add_homogenous_args(
            self.simparams_opt.len(),
            ctx.bool_ty().as_basic_type_enum(),
            &mut args,
        );
        let ports_connected = add_homogenous_args(
            self.ports_connected.len(),
            ctx.bool_ty().as_basic_type_enum(),
            &mut args,
        );

        let temperature = if self.temperature {
            let res = args.len();
            args.push(ctx.real_ty().as_basic_type_enum());
            Some(res)
        } else {
            None
        };

        let parameters = args.len()..args.len() + self.parameters.len();
        args.extend(
            self.parameters
                .iter()
                .map(|para| ctx.cabi_parameter_ty(ctx.mir[*para].ty)),
        );

        let inputs: Vec<usize> = self
            .inputs
            .iter()
            .map(|(_, local)| {
                let res = args.len();
                let ty = ctx.cabi_parameter_ty(self.cfg.locals[*local].ty);
                args.push(ty);
                res
            })
            .collect();

        let function = {
            let args: Vec<_> = args.iter().map(|it| (*it).into()).collect();
            ctx.module.add_function(
                &self.ffi_name,
                ctx.cabi_parameter_ty(self.cfg.locals[self.output].ty)
                    .fn_type(&args, false),
                Some(Linkage::External),
            )
        };

        let entry = ctx.context.append_basic_block(function, "entry");

        let cg = ctx.cfg_codegen(&self.cfg, function);
        cg.ctx.builder.position_at_end(entry);

        let mut paras: Vec<_> = function.get_params();

        for (idx, param) in parameters.clone().zip(&self.parameters) {
            let ty = cg.ctx.mir[*param].ty;
            ty.with_info(|info| {
                if !info.dimensions.is_empty() {
                    let ty = cg.ctx.ty_from_info(info);
                    let ptr_ty = ty.ptr_type(AddressSpace::Generic);
                    let ptr = cg.ctx.builder.build_pointer_cast(
                        paras[idx].into_pointer_value(),
                        ptr_ty,
                        "abi_to_local",
                    );
                    paras[idx] = cg.ctx.builder.build_load(ptr, "abi_to_local")
                }
            })
        }

        let param = |indecies: Range<usize>| indecies.map(|n| paras[n]).collect();

        let codegen_data = ExtractionCodeGen {
            extraction: self,
            parameters: param(parameters),
            voltages: param(voltages),
            currents: param(currents),
            simparams: param(simparams),
            simparams_str: param(simparams_str),
            simparams_given: param(simparams_given),
            port_connected: param(ports_connected),
            temperature: temperature.map(|n| paras[n]),
        };

        let mut cg = cg.attach_call_type_data(codegen_data);
        cg.alloc_vars_and_branches(|_, _, _| unreachable!());

        for ((_, local), arg) in self.inputs.iter().zip(inputs) {
            let value = cg.cfg.locals[*local].ty.with_info(|info| {
                if info.dimensions.is_empty() {
                    paras[arg]
                } else {
                    let ty = cg.ctx.ty_from_info(info);
                    let ptr_ty = ty.ptr_type(AddressSpace::Generic);
                    let ptr = cg.ctx.builder.build_pointer_cast(
                        paras[arg].into_pointer_value(),
                        ptr_ty,
                        "abi_to_local",
                    );
                    cg.ctx.builder.build_load(ptr, "abi_to_local")
                }
            });
            cg.local_values[*local] = LocalValue::Value(value)
        }

        cg.ctx
            .builder
            .build_unconditional_branch(cg.blocks[START_BLOCK]);

        cg
    }
}

pub struct ExtractionCodeGen<'a, 'c> {
    extraction: &'a VariableFunction,
    parameters: Vec<BasicValueEnum<'c>>,
    voltages: Vec<BasicValueEnum<'c>>,
    currents: Vec<BasicValueEnum<'c>>,
    simparams: Vec<BasicValueEnum<'c>>,
    simparams_str: Vec<BasicValueEnum<'c>>,
    simparams_given: Vec<BasicValueEnum<'c>>,
    port_connected: Vec<BasicValueEnum<'c>>,
    temperature: Option<BasicValueEnum<'c>>,
}

impl<'a, 'c> ExtractionCodeGen<'a, 'c> {
    fn access<T: PartialEq + Copy>(
        val: T,
        arg: &[BasicValueEnum<'c>],
        position: &[T],
    ) -> BasicValueEnum<'c> {
        *position
            .iter()
            .zip(arg)
            .find(|(x, _)| *x == &val)
            .unwrap()
            .1
    }
}

impl<'lt, 'c> CallTypeCodeGen<'lt, 'c> for VaeFuntions {
    type CodeGenData = ExtractionCodeGen<'lt, 'c>;

    fn read_input<'a, A: CfgFunctions>(
        cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        input: &Self::I,
    ) -> BasicValueEnum<'c> {
        let ctx = &cg.call_type_data;

        match input {
            DefaultInputs::Parameter(ParameterInput::Value(param)) => {
                ExtractionCodeGen::access(*param, &ctx.parameters, &ctx.extraction.parameters)
            }

            DefaultInputs::Parameter(ParameterInput::Given(_)) => {
                cg.ctx.bool_ty().const_all_ones().into()
            }

            DefaultInputs::PortConnected(PortConnected(port)) => ExtractionCodeGen::access(
                *port,
                &ctx.port_connected,
                &ctx.extraction.ports_connected,
            ),

            DefaultInputs::SimParam(SimParam {
                name,
                kind: SimParamKind::Real,
            }) => ExtractionCodeGen::access(*name, &ctx.simparams, &ctx.extraction.simparams),

            DefaultInputs::SimParam(SimParam {
                name,
                kind: SimParamKind::RealOptional,
            }) => ExtractionCodeGen::access(
                *name,
                &ctx.simparams[ctx.extraction.simparams.len()..],
                &ctx.extraction.simparams_opt,
            ),

            DefaultInputs::SimParam(SimParam {
                name,
                kind: SimParamKind::String,
            }) => {
                ExtractionCodeGen::access(*name, &ctx.simparams_str, &ctx.extraction.simparams_str)
            }

            DefaultInputs::SimParam(SimParam {
                name,
                kind: SimParamKind::RealOptionalGiven,
            }) => ExtractionCodeGen::access(
                *name,
                &ctx.simparams_given,
                &ctx.extraction.simparams_opt,
            ),

            DefaultInputs::CurrentProbe(CurrentProbe(branch)) => {
                ExtractionCodeGen::access(*branch, &ctx.currents, &ctx.extraction.currents)
            }

            DefaultInputs::Voltage(Voltage { hi, lo }) => {
                ExtractionCodeGen::access((*hi, *lo), &ctx.voltages, &ctx.extraction.voltages)
            }
            DefaultInputs::Temperature(_) => ctx.temperature.unwrap(),
            DefaultInputs::PartialTimeDerivative(_) => {
                cg.ctx.real_ty().const_float(0.0).as_basic_value_enum()
            }
        }
    }

    fn gen_call_rvalue<'a, A: CfgFunctions>(
        &self,
        _cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        _args: &IndexSlice<CallArg, [BasicValueEnum<'c>]>,
    ) -> BasicValueEnum<'c> {
        match *self {}
        // cg.ctx.real_ty().const_float(0.0).as_basic_value_enum()
    }

    fn gen_call<'a, A: CfgFunctions>(
        &self,
        _cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        _args: &IndexSlice<CallArg, [BasicValueEnum<'c>]>,
    ) {
        match *self {}
    }

    fn gen_limexp<'a, A: CfgFunctions>(
        cg: &mut CfgCodegen<'lt, 'a, 'c, Self::CodeGenData, A, Self>,
        arg: BasicValueEnum<'c>,
    ) -> BasicValueEnum<'c> {
        cg.ctx.build_intrinsic_call(Intrinsic::Exp, &[arg])
    }
}
