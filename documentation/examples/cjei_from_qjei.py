# test the HiCUM/L2 model with verilog-ae using DMT and the HiCUM/L2 verilog code
# both DMT and HiCUM are proprietary, this test case may only be done if you have access to both of them.
#test case to ensure verilog-ae interface
try:
    import hl2
except ModuleNotFoundError:
    print('Warning: hl2 module from verilog-ae not found')

import numpy as np
import matplotlib.pyplot as plt
import os

def plot_comp(x, y_model, y_sim, name, x_label, y_label, y_log=False,comparsion='Simulator'):
    """ Plots the model versus the simulation for visual comparision
    """
    _fig = plt.figure()
    #plt.title(name , fontsize=20)
    plt.xlabel(x_label, fontsize=13)
    plt.ylabel(y_label, fontsize=13)
    plt.plot(x, y_model ,label=r'\tiny  Verilog-AE',markevery=30,linestyle=' ',marker='x')
    plt.plot(x, y_sim  ,label=comparsion,linestyle='-')
    if y_log:
        plt.yscale('log')
    #plt.legend()

#vae outputs
vb    = np.linspace(0,0.4,1001)
vc    = np.linspace(0,1,1001)
ve    = np.zeros_like(vb)
temp  = np.linspace(300,400,1001)

operating_point = {
    'br_biei' : vb-ve,
    'br_bici' : vb-vc,
    'br_bpei' : vb-ve,
    'br_bpci' : vb-vc,
}

modelcard_vae = {name: param.default for (name,param) in hl2.modelcard.items()}
modelcard_vae['cjei0'] = 8.869e-12
modelcard_vae['ajei'] = 1.65
modelcard_vae['tnom'] = 26.85
modelcard_vae['vdei'] = 0.714
modelcard_vae['vgb'] = 0.91
modelcard_vae['zei'] = 0.2489


cjei_manual_vae = hl2.functions['Cjei'].eval(temperature = temp,voltages=operating_point, **modelcard_vae)
cjei_vae = hl2.functions['Qjei_ddV'].eval(temperature = temp,voltages=operating_point, **modelcard_vae)


#check models
# assert np.allclose(cjei_manual_vae,cjei_vae)
print(cjei_manual_vae)
print(cjei_vae)
plot_comp(x=vb-ve,y_model=cjei_manual_vae*1e12,y_sim=cjei_vae*1e12,x_label=r'$V_{\mathrm{BE}}\left( \mathrm{V}  \right)$',y_label=r'$C_{\mathrm{jei}}\left( \mathrm{pF} \right)$',name=r'$C_{\mathrm{jei}}(V_{\mathrm{BE}})$',comparsion=r"HICUM")
plt.show()