import pickle
import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import hl2

# this example demonstrates how to extract the low bias transit time parameters t0 dt0h and tbvl
# The HiCUM t0 model depends on the cjcx model parameters, which are thus extracted first.

###########################
#### Cjcx extraction ######
###########################

## Step 1: Load measurement data
data_path = os.path.join('tests','test_data','example_meas_data.pckle')
with open(data_path, 'rb') as path:
    df = pickle.load(path)

vbc  = df['cjcx']['vbc']
cjcx = df['cjcx']['cjcx']

#load verilog-AE model
cjcx_model = hl2.functions['CjCx_i'].eval #assuming fbcpar = 0 Cjcx_ii is always 0 so Cjcx = Cjcx_i
modelcard = {name: param.default for (name, param) in hl2.modelcard.items()}

modelcard.pop('cjcx0')
modelcard.pop('cbcpar')
modelcard.pop('vptcx')
modelcard.pop('vdcx')
modelcard.pop('zcx')

#initial guess
i_0           = np.argmin(np.abs(vbc-0))
initial_guess = [cjcx[i_0]/2  , cjcx[i_0]/2  ,  1, 0.3 , 0.3]
#preparation of data
#optimize using Verilog-AE extracted function
def model_cjcx(vbc, cjcx0, cbcpar, vptcx, vdcx, zcx):
    return cjcx_model(temperature=298, voltages = {'br_bci':vbc}, cjcx0=cjcx0, cbcpar=cbcpar, vptcx=vptcx, vdcx=vdcx, zcx=zcx,**modelcard)

popt, _cov = curve_fit(model_cjcx,vbc,cjcx,p0=initial_guess)
#postprocessing

#write back results
modelcard['cjcx0']  = popt[0]
modelcard['cbcpar'] = popt[1]
modelcard['vptcx']  = popt[2]
modelcard['vdcx']   = popt[3]
modelcard['zcx']    = popt[4]

###########################
####   T0 extraction ######
###########################

data  = df['t0']

# step 1: convert the data to 1D
gmi_inv_all, tau_all, vbc_all = [], [], []
for line in data.values():
    gmi_inv = 1/line['gmi']
    filter_ = np.logical_and(30<gmi_inv, 100>gmi_inv)
    gmi_inv_all.append(1/(line['gmi'][filter_]))
    tau_all.append(1/(2*np.pi*line['fti'][filter_]))
    vbc_all.append(np.ones_like(line['gmi'][filter_])*line['vbcf'])

gmi_inv  = np.hstack(gmi_inv_all)
tau      = np.hstack(tau_all)
vbc      = np.hstack(vbc_all)

# step 2: optimization

#initial guess
initial_guess = [1e-12  , 1e-12, 1e-12, 1e-12/10]

#preparation of data
modelcard.pop('tbvl')
modelcard.pop('dt0h')
modelcard.pop('t0')
tauf0_model = hl2.functions['T_f0'].eval

#optimize using verilog-AE extracted function
def model_tau_gmi(gmi_inv, t0, dt0h, tbvl, m):
    t0 = tauf0_model(temperature=298, voltages={'br_bici':vbc}, t0=t0, dt0h=dt0h,tbvl=tbvl, **modelcard)
    return t0 + m*gmi_inv
popt, _cov = curve_fit(model_tau_gmi,gmi_inv,tau,p0=initial_guess)
#postprocessing

#write back results
modelcard['t0']   = popt[0]
modelcard['dt0h'] = popt[1]
modelcard['tbvl'] = popt[2]
modelcard['m']    = popt[3]

#redefine function for plotting
def model_tau_gmi(gmi_inv, vbc, m=None, **kwargs):
    t0 = tauf0_model(temperature=298,voltages={'br_bici':vbc},**kwargs)
    return t0 + m*gmi_inv

# plotting
for line,marker,color in zip(data.values(),['x','s','o','^','<','d','_','.'],['k','c','g','y','m','r','b','gray']):
    gmi_inv = 1/line['gmi']
    tau     = 1/(2*np.pi*line['fti'])
    vbcf    = line['vbcf']
    plt.plot(gmi_inv,model_tau_gmi(gmi_inv, vbcf, **modelcard)*1e12,linestyle='-', color=color)
    plt.plot(gmi_inv,tau*1e12,label=r'$V_{{\mathrm{{BC}}}}={0:1.1f}$'.format(vbcf),linestyle=' ',marker=marker, color=color, markevery=0.05)

#plt.plot(vbc,model.model_cjcx_temp(vbc,t_dev=298,**modelcard)*1e15,label='Verilog-AE',linestyle='-')
plt.ylabel(r'$1/\left( 2 \pi f_{\mathrm{ti}}\right)\left( \mathrm{ps} \right) $')
plt.xlabel(r'$1/g_{\mathrm{mi}} \left( 1/\mathrm{S}  \right) $')
plt.legend()
plt.xlim((0,70))
plt.ylim((0,1.5))
plt.show()

# tikzplotlib.save(os.path.join('tests','test_plots','t0_extraction.tex'),axis_width=fR'\linewidth',strict=False)