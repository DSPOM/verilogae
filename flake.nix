{
  description = "Flake for the VerilogAE compiler: A compiler for VerilogA files that generates a python interface for compact model extraction";


  inputs.gitignore = {
    type = "github";
    owner = "hercules-ci";
    repo = "gitignore.nix";
    flake = false;
  };

  inputs.rust = {
    type = "github";
    owner = "oxalica";
    repo = "rust-overlay";
  };


  outputs = { self, nixpkgs, gitignore, rust,  ... }:
    let

      nameValuePair = name: value: { inherit name value; };
      genAttrs = names: f: builtins.listToAttrs (map (n: nameValuePair n (f n)) names);
      allSystems = [ "x86_64-linux" "aarch64-linux" "i686-linux" "x86_64-darwin" ];


      forSystems = systems: f: genAttrs systems (
        system: f rec {
          inherit system;
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              rust.overlay
            ];
          };
          dep = import packaging/nix/dependencies.nix { inherit pkgs; lib=nixpkgs.lib; };
        }
      );


      forAllSystems = f: forSystems allSystems f;
    in
      {
        devShell = forAllSystems (
          { pkgs, dep, ... }:
            with dep;
            pkgs.mkShell {
              name = "verilogae";

              inherit buildInputs LLVM_SYS_120_PREFIX;


              nativeBuildInputs = with pkgs; dep.nativeBuildInputs ++ [
                rust-bin.stable.latest.default
                python38
                tokei
                crate2nix
                cargo-outdated
                cargo-edit
                cargo-flamegraph
              ];
            }
        );

        packages =
          forAllSystems
            (
              { system, pkgs, dep, ... }:
                let
                  cargoNix = import ./Cargo.nix {
                    inherit pkgs;
                    defaultCrateOverrides = pkgs.defaultCrateOverrides // {
                      llvm-sys = { ... }:
                        with dep;
                        {
                          inherit buildInputs LLVM_SYS_120_PREFIX;
                        };
                      libmimalloc-sys = { ... }:
                        with dep;
                        {
                          inherit nativeBuildInputs;
                        };

                      verilogae = { ... }:
                        with dep;
                        {
                          inherit buildInputs;
                        };
                    };
                  };

                  buildVaeWheel = self.lib.buildWheel { inherit pkgs; verilogae = self.packages.${system}.verilogae; };

                  # deb_args_64 = {
                  #   diskImage = pkgs.vmTools.diskImageFuns.debian9x86_64 {};
                  #   src = self.packages.${system}.verilogae.src;
                  #   name = "verilogae-deb";
                  #   buildInputs = [];
                  #   meta.description = "A compiler for VerilogA files that generates a python interface for compact model extraction";
                  # };
                  # deb_args_32 = {
                  #   diskImage = pkgs.vmTools.diskImageFuns.debian9xi686 {};
                  #   src = self.packages.${system}.verilogae.src;
                  #   name = "verilogae-deb";
                  #   buildInputs = [];
                  #   meta.description = "A compiler for VerilogA files that generates a python interface for compact model extraction";
                  # };
                in
                  {
                    verilogae = cargoNix.workspaceMembers.verilogae.build;
                    diode_example_whl = buildVaeWheel {
                      src = ./documentation/examples;
                      mainfile = "diode.va";
                      pname = "diode";
                      cargoHash = "sha256-JPOBdgGqlMLGZFBAFoBZqL+GwTkzD5BkyPouy8b9JK4=";
                    };
                  }
              # currently doesnt work
              # //
              # nixpkgs.lib.optionalAttrs (system == "x86_64-linux") {

              #   verilogae_deb = pkgs.releaseTools.debBuild deb_args_64;
              #}
              #//
              # nixpkgs.lib.optionalAttrs (system == "i686-linux") {

              #   verilogae_deb_32 = pkgs.releaseTools.debBuild deb_args_32;
              #}
              # hello
            );



            defaultPackage = forAllSystems ({ system, ... }: self.packages.${system}.verilogae);

            overlay = final: prev: { verilogae = self.defaultPackage.x86_64-linux;};

        lib = {
          buildWheel = import ./packaging/nix/build_vae_wheel.nix;
        };


      };

}
