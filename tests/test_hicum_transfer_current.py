#test of the hicum transfer current model extracted by verilog-ae using ADS circuit simulations
#ADS circuit simulations not needed to run this test, data is included in Git repo
""" Testing model equations versus Verilog-Simulation
"""
import tikzplotlib
import os.path
import numpy as np
import matplotlib.pyplot as plt
import pickle

try:
    import hl2
except ModuleNotFoundError:
    print('Warning: hl2 module from verilog-ae not found')

ads_avail = False #set per Hand
dmt_avail = True

try:
    from DMT.core import DutType, SimCon, Sweep, specifiers
    from DMT.config import DB_DIR
    from DMT.ADS import DutAds
    from DMT.hl2 import McHicum, Hl2V2P4P0Model
except ModuleNotFoundError:
    dmt_avail = False



#some paths
path_df_sim_tau   = os.path.join('.','tests','test_data','df_sim_tau.pckl')
path_df_sim_gummel= os.path.join('.','tests','test_data','df_sim_gummel.pckl')
path_verilog      = os.path.join('.','tests','hl2.va') #verilog file for ADS circuit simulation!
path_modelcard    = os.path.join('.','tests','test_data','npn_full_sh.lib')

#convert to abs paths
path_modelcard = os.path.abspath(path_modelcard)
path_verilog   = os.path.abspath(path_verilog)

#load modelcard
if dmt_avail:
    mc             = McHicum(load_model_from_path=path_modelcard)
    # mc.set_values({'flsh':0})
    mc.set_values({'flsh':0,'rbx':0, 're':1,'rbi0':0,'rcx':0}) #turn off series resistances
    mc.read_va_file_boundaries_opvars(path_verilog)
    model_dmt      = Hl2V2P4P0Model()

def test_tf0_model():
    """ Compares the tf0 model versus the simulation.
    """
    if dmt_avail and ads_avail:
        df_simulated = get_simulation_data(mc)
        with open(path_df_sim_tau, 'wb') as df_path:
            pickle.dump(df_simulated, df_path)
    else:
        with open(path_df_sim_tau, 'rb') as df_path:
            df_simulated = pickle.load(df_path)

    vbc        = df_simulated['_V_BiE'].to_numpy()-df_simulated['_V_CiE'].to_numpy() #voltage at the actual nodes
    tau_f0_sim = df_simulated['T_f0'].to_numpy()

    tau_f0_model = hl2.functions['T_f0'].eval(
        temperature=300,
        voltages={'br_bici':vbc},
        **mc.to_kwargs()
    )
    assert np.allclose(tau_f0_model, tau_f0_sim)
    plot_comp(vbc, tau_f0_model, tau_f0_sim, "tau_f0(V_BC)", x_label=r'$V_{\mathrm{BC}}$', y_label=r'$\tau_{\mathrm{f0}}$')

def test_it_model():
    """ Compares the tf0 model versus the simulation.
    """
    if dmt_avail and ads_avail:
        df_simulated = get_simulation_data(mc, sweep_type='gummel')
        with open(path_df_sim_gummel, 'wb') as df_path:
            pickle.dump(df_simulated, df_path)
    else:
        with open(path_df_sim_gummel, 'rb') as df_path:
            df_simulated = pickle.load(df_path)

    vbc        = df_simulated['_V_BiE'].to_numpy()-df_simulated['_V_CiE'].to_numpy() #voltage at the actual nodes
    vbe        = df_simulated['_V_BiE'].to_numpy()-df_simulated['_V_EiE'].to_numpy() #voltage at the actual nodes
    itf_sim     = df_simulated['itf'].to_numpy()
    temperature      = df_simulated['TK'].to_numpy()

    itf_model = hl2.functions['itf'].eval(
        temperature = temperature,
        voltages={'br_bici':vbc,'br_biei':vbe},
        **mc.to_kwargs()
    )

    assert np.allclose(itf_model, itf_sim)
    plot_comp(vbe, itf_model*1e3, itf_sim*1e3, "$I_{TF}(V_{\mathrm{BE}})$", x_label=r'\tiny $V_{\mathrm{BE}}\left( \mathrm{V}  \right)$', y_label=r'\tiny $I_{\mathrm{TF}}\left( \mathrm{mA} \right)$',y_log=True)

def test_gmi_model():
    """ Compares the tf0 model versus the simulation.
    """
    if dmt_avail and ads_avail:
        df_simulated = get_simulation_data(mc, sweep_type='gummel')
        with open(path_df_sim_gummel, 'wb') as df_path:
            pickle.dump(df_simulated, df_path)
    else:
        with open(path_df_sim_gummel, 'rb') as df_path:
            df_simulated = pickle.load(df_path)

    vbc        = df_simulated['_V_BiE'].to_numpy()-df_simulated['_V_CiE'].to_numpy() #voltage at the actual nodes
    vbe        = df_simulated['_V_BiE'].to_numpy()-df_simulated['_V_EiE'].to_numpy() #voltage at the actual nodes
    gmi_sim     = df_simulated['GMi'].to_numpy()
    temperature      = df_simulated['TK'].to_numpy()

    gmi_model = hl2.functions['GMi'].eval(
        temperature = temperature,
        voltages={'br_bici':vbc,'br_biei':vbe},
        **mc.to_kwargs()
    )

    plot_comp(vbe, gmi_model*1e3, gmi_sim*1e3, "$gmi(V_{\mathrm{BE}})$", x_label=r'$V_{\mathrm{BE}}\left( \mathrm{V}  \right)$', y_label=r'$gmi\left( \mathrm{mS} \right)$',y_log=True)


def get_simulation_data(mc, sweep_type='vbc_sweep'):
    """ Simulates a Dut wit the given modelcard and returns the read dataframe.
    """
    dut = DutAds(DB_DIR, DutType.npn, mc, nodes='C,B,E,S,T', reference_node='E', copy_va_files = True)

    # create a sweep
    if sweep_type=='vbc_sweep':
        sweepdef  =  [
            {'var_name':specifiers.FREQUENCY, 'sweep_order':4, 'sweep_type':'LOG' , 'value_def':[8,9,2]},
            {'var_name':specifiers.VOLTAGE+'B', 'sweep_order':3, 'sweep_type':'LIN' , 'value_def':[0.6,0.7,3]},
            {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':3, 'sweep_type':'SYNC', 'master':specifiers.VOLTAGE+'B', 'offset':'V_CB'},
            {'var_name':specifiers.VOLTAGE+['C', 'B'], 'sweep_order':2, 'sweep_type':'LIST', 'value_def':[-0, -0.5, 0.5, 1.0]},
            {'var_name':specifiers.VOLTAGE+'E', 'sweep_order':1, 'sweep_type':'CON' , 'value_def':[0]},
        ]
        outputdef=['I_C','I_B']
        othervar={'TEMP':300,'w':10,'l':0.25}
        sweep  =  Sweep('gummel', sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)
    elif sweep_type=='gummel':
        sweepdef  =  [
            {'var_name':specifiers.VOLTAGE+'B', 'sweep_order':3, 'sweep_type':'LIN' , 'value_def':[0.6,1.1,51]},
            {'var_name':specifiers.VOLTAGE+'C', 'sweep_order':3, 'sweep_type':'SYNC', 'master':specifiers.VOLTAGE+'B', 'offset':'V_CB'},
            {'var_name':specifiers.VOLTAGE+['C', 'B'], 'sweep_order':2, 'sweep_type':'LIST', 'value_def':[0]},
            {'var_name':specifiers.VOLTAGE+'E', 'sweep_order':1, 'sweep_type':'CON' , 'value_def':[0]},
        ]
        outputdef=['I_C','I_B']
        othervar={'TEMP':300,'w':10,'l':0.25}
        sweep  =  Sweep('gummel', sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)

    sim_con = SimCon(t_max=40)
    sim_con.append_simulation(dut, sweep)
    sim_con.run_and_read(force=False)
    return dut.get_data(sweep=sweep)

def plot_comp(x, y_model, y_sim, name, x_label, y_label, y_log=False,comparsion='Simulator'):
    """ Plots the model versus the simulation for visual comparision
    """
    _fig = plt.figure()
    #plt.title(name , fontsize=20)
    plt.xlabel(x_label, fontsize=13)
    plt.ylabel(y_label, fontsize=13)
    plt.plot(x, y_model ,label='Verilog-AE',markevery=3,linestyle=' ',marker='x')
    plt.plot(x, y_sim  ,label=comparsion,linestyle='-')
    if y_log:
        plt.yscale('log')
    #plt.legend()


if __name__ == '__main__':
    test_tf0_model()
    test_gmi_model()
    test_it_model()

    plt.show()
